#
# FastAPI nameshortener example
#

FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

ENV PORT="8080"
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV WORKERS_PER_CORE=1
ENV MAX_WORKERS=2

COPY ./app /app

RUN pip install -r /app/requirements.txt

WORKDIR /app