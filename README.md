# FastShortener

Example of FastAPI Deployment in Docker 

![gif](images/fastapi_codeengine.gif)

## Dockerfile

Using the [uvicorn-gunicorn-fastapi](https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker) base image, using [poetry](https://python-poetry.org/) for the dependencies.

## Local test build & run

Build with e.g.

```
docker build . -t fast
```

Run with

```
docker run --rm -p 8080:8080 fast
```