from fastapi.testclient import TestClient

from .main import app

client = TestClient(app)

def test_shorten_description():
    response = client.get("/shorten/Freundschaftsbeziehungen")
    assert response.status_code == 200
    response_dict = response.json()
    assert len(response_dict['short']) <= 14