import pyphen

dic = pyphen.Pyphen(lang='de_DE')

def german_shortener_v1( german_word  ):
    """
        Basic shortener; 
            - Keeps the first syllable intact
            - For remaining syllable: only add 1st letter
    """
    global dic
    
    german_word = german_word.replace('-','')

    hyphened = dic.inserted(german_word)
    german_word_list = hyphened.split('-')
    
    short = german_word_list[0] + "".join([ w[0] for w in german_word_list[1:]])
    short = short.upper()
    
    return short

def german_shortener_v2( german_word , maximum_first_syllable=4):
    """
        Keep the last letter of the last syllable, if more than 2 syllables
    """
    global dic
    
    german_word = german_word.replace('-','')

    hyphened = dic.inserted(german_word)
    german_word_list = hyphened.split('-')
    
    short = german_word_list[0][:maximum_first_syllable] + "".join([ w[0] for w in german_word_list[1:]])
    
    if german_word_list[1:]:
        short += german_word_list[-1][-1]
    
    
    short = short.upper()
    
    return short

def german_shortener_v3( german_word , maximum_first_syllable=4):
    """
        - First Syllable: Maximum length maximum_first_syllable
        - First Syllable: Takes last character of first_syllable
        - Remaining Syllable: Only use first character
        - Add last character from last syllable
    """
    global dic
    
    german_word = german_word.replace('-','')

    hyphened = dic.inserted(german_word)
    german_word_list = hyphened.split('-')
    
    first_syllable = german_word_list[0][:maximum_first_syllable - 1]
    
    if len(german_word_list[0]) >= maximum_first_syllable:
        first_syllable += german_word_list[0][-1]
    
    short = first_syllable + "".join([ w[0] for w in german_word_list[1:]])
    
    if german_word_list[1:]:
        short += german_word_list[-1][-1]
    
    
    short = short.upper()
    
    return short