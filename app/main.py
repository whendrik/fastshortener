from typing import Optional

from fastapi import FastAPI

from shortener import german_shortener_v3

app = FastAPI()


@app.get("/")
def read_root():
    return {"status": "online"}


@app.get("/shorten/{long_description}")
def shorten_description(long_description: str):
    return {"original": long_description, "short": german_shortener_v3(long_description)}

